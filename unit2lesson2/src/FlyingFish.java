class FlyingFish extends Animal implements Flyable{
    {
        helloMessage = ".....";
    }
    @Override
    public void fly(){
        System.out.println("jump and swim");
    }
}
