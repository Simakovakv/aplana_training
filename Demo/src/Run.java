/**
 * Есть входной файл с набором слов, написанных через пробел
 * Необходимо:
 * Прочитать слова из файла.
 * Отсортировать в алфавитном порядке.
 * Посчитать сколько раз каждое слово встречается в файле.
 * Вывести статистику на консоль
 * Найти слово с максимальным количеством повторений.
 * Вывести на консоль это слово и сколько раз оно встречается в
 * файле
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Run {

    public static void main(String[] args) {

        Scanner input;

        try {

            input = new Scanner(new File("example.txt"));

            Map<String, Integer> statistics = new TreeMap<>();

            int max = 0;

            while (input.hasNext()) {

                String word = input.useDelimiter("[.,:;()?!\"\\s]+|\\s+-\\s+").next().toLowerCase(); //to ignore all signs and spaces

                Integer count = statistics.get(word);

                if (count == null) {

                    count = 0;

                }

                statistics.put(word, ++count);

                if(count > max) {

                    max = count;

                }

            }

            System.out.println(statistics);

            System.out.printf("Наиболее частое слово(a) в тексте:\n");


            for (Map.Entry e : statistics.entrySet()){

                if (e.getValue().equals(max))

                    System.out.printf("%s - встречается %d раз(а))\n", e.getKey(), max);

            }

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        } catch (Exception ex) {

            ex.printStackTrace();

        }
    }
}