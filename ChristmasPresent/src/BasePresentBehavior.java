public interface BasePresentBehavior {

    String getName();

    double getWeight();

    double getCost();

    String print();

}
