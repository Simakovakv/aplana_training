import java.util.Scanner;

public class Cake extends BasePresent {

    private String sizeCake;

    private String shapeCake;

    Cake(String name){
        super(name);
        System.out.println("Введите размер:");
        this.sizeCake = new Scanner(System.in).next();
        System.out.println("Введите форму:");
        this.shapeCake = new Scanner(System.in).next();

    }

    Cake(String name, int weight, int cost, String sizeCake, String shapeCake){

        super(name, weight, cost);

        this.sizeCake = "Large";

        this.shapeCake = "Circle";

    }

    String getSize() {

        return this.sizeCake;

    }

    String getShape() {

        return this.shapeCake;

    }

    public String print() {

        return super.print() + "Форма: " + this.shapeCake + " Размер: " + this.sizeCake;

    }
}
