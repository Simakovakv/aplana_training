/**
 * This programm can help you to get Sweet Present with 2 ways
 *
 * First way you set max weight of Present and next set parameters of it,
 * When capacity of box is over, the program will be stopped
 *
 * Second way you set max weight of Present, but you maybe want to get the most
 * profitable and expensive present. Then you set different sweet presents and program
 * advanced you only profitable present which fit in the box (analog Continuous knapsack task)
 *
 * Input:
 * 1,2
 * Макс вес: (любое число)
 * тип подарка: 1-5
 * стоимость: (любое число)
 * вес: (любое число)
 * Тип (любая строка)
 * Процент какао (любое число)
 * Форма (любая строка)
 * Размер (любая строка)
 * Вкус(Любая строка)
 *
 * Output:
 * 1 way: Товар не добавлен. Перевес. Оставшееся место в коробке: 350
 * Всего подарков: 1
 * Общий вес подарков: 150
 * Общая стоимость подарков: 250
 * Вы выбрали следующие продукты:
 *
 * Название: Chocolate Вес: 150.0 Цена: 250.0 Тип: Молосный Процент какао: 25
 *
 * Output:
 * 2 way:
 * Лучший подарок:
 * Название: Candy Вес: 20.0 Цена: 456.0 Тип:Truffle
 * Название: Chocolate Вес: 150.0 Цена: 350.0 Тип: Milk Процент какао: 98
 * Название: Chocolate Вес: 260.0 Цена: 540.0 Тип: Dark Процент какао: 98
 * Всего подарков: 3
 * Общий вес подарков: 430
 * Общая стоимость подарков: 1346
 */

public class App {

    public static void main(String... Agrs) {

        while(true) {

            try {

                new UserDialog();

                break;

            } catch (NumberFormatException e) {

                System.out.println("Вы должны ввести число: " + e);

            } catch (NullPointerException e) {

                System.out.println(e);

            }
        }

    }
}
