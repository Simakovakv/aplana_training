public class FactoryPresent {

    public static BasePresentBehavior createPresent(String name) {

        switch(name) {
            case "1":
                return new Chocolate("Chocolate");
            case "2":
                return new Candy("Candy");
            case "3":
                return new Cake("Chocolate");
            case "4":
                return new ChupaChups("ChupaChups");
            case "5":
                return new JellyBean("JellyBean");
        }
    return null;
    }
}
