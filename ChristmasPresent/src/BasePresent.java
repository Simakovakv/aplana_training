import java.util.Scanner;

public abstract class BasePresent implements BasePresentBehavior {

    private String name;

    private double weight;

    private double cost;

    BasePresent(String name){
        Scanner reader = new Scanner(System.in);
        this.name = name;
        System.out.print("Введите стоимость: ");
        this.cost = Double.valueOf(reader.next());
        System.out.print("Введите вес: ");
        this.weight = Double.valueOf(reader.next());
    }

    BasePresent(String name, double weight, double cost) {
        this.name = name;
        this.weight = weight;
        this.cost = cost;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getWeight() {
        return this.weight;
    }

    @Override
    public double getCost() {
        return this.cost;
    }

    @Override
    public String print(){
        return "Название: " + this.name + " Вес: " + this.weight + " Цена: " + this.cost;
    }

}
