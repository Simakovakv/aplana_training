import java.util.Scanner;

public class ChupaChups extends BasePresent {

    private String taste;

    ChupaChups(String name){
        super(name);
        System.out.println("Введите вкус:");
        this.taste = new Scanner(System.in).next();

    }


    ChupaChups(String name, int weight, int cost, String taste){

        super(name, weight, cost);

        this.taste = "Caramel";

    }

    String getTaste() {

        return this.taste;
    }

    public String print() {
        return super.print() + " Вкус:" + this.taste;
    }



}
