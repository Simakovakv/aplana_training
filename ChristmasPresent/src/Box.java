import java.util.*;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Box implements BoxBehavior {

    private int maxWeight;

    private int cost = 0;

    private int weight = 0;

    private List<BasePresentBehavior> listPresent;

    Box(int maxWeight) {
        this.listPresent = new ArrayList<BasePresentBehavior>();
        this.maxWeight = maxWeight;
    }

    public void/*String*/ display() {
        for (BasePresentBehavior present:listPresent) {
            System.out.println(present.print());
        }
    }

    public int getMaxWeight() {
        return this.maxWeight;
    }

    public int getCount () {
/*        int counter = 0;
        for(BasePresentBehavior present:listPresent){
            if(present!=null) {
                counter++;
            }
        }
        return counter;*/
        return listPresent.size();

    }

    public int getCount (String name){
        int counter = 0;
        for (BasePresentBehavior p : listPresent) {
            if (name == p.getName()) {
                counter++;
            }
        }
        return counter;
    }

    public boolean appendToList(BasePresentBehavior basePresent) {

            listPresent.add(basePresent);

            cost += basePresent.getCost();

            weight += basePresent.getWeight();

            return true;

    }

    public boolean checkWeight(BasePresentBehavior basePresent) {

        if ((basePresent.getWeight() + weight) <= getMaxWeight())
            return true;
        else
            System.out.printf("Товар не добавлен. Перевес. Оставшееся место в коробке: %d \n", getMaxWeight() - weight);
        return false;
    }

    public void chooseBestCost() {
        listPresent.sort(
                Comparator.comparingDouble(x -> x.getCost() / x.getWeight()));

        Collections.reverse(listPresent);


        //another sort
        /*       listPresent.sort((x1, x2) -> {
            return x1.getCost() / x1.getWeight() - (x2.getCost() / x2.getWeight();
        });*/
        double capacity = getMaxWeight();
        List<Integer> id= new ArrayList<>();


        for (int i = 0; i < listPresent.size(); i++) {

            if ((capacity - listPresent.get(i).getWeight()) >= 0)
            {
                System.out.println(listPresent.get(i).print());
                capacity -= listPresent.get(i).getWeight();
            }
            else {
                cost -= listPresent.get(i).getCost();
                weight -= listPresent.get(i).getWeight();
                id.add(i);
                //continue;
            }

        }
        int x = 0;
        for(int i:id) {
            listPresent.remove(i-x);
            x++;
        }
    }

    public int getCommonWeight(){
//        int weight = 0;
//        for (BasePresentBehavior present : listPresent) {
//            weight += present.getWeight();
//        }
        return weight;
    }
    public int getCommonCost(){
//        int cost = 0;
//        for (BasePresentBehavior present : listPresent) {
//            cost += present.getCost();
//        }
        return cost;
    }


}
