import java.util.Scanner;

public class Chocolate extends BasePresent {

    private int percentageCocoa;
    private String typeChocolate;

    Chocolate(String name){
        super(name);
        System.out.println("Введите тип:");
        this.typeChocolate = new Scanner(System.in).next();
        System.out.println("Введите процент какао: ");
        this.percentageCocoa = Integer.valueOf(new Scanner(System.in).next());

    }

    Chocolate(String name, int weight, int cost){
        super(name, weight, cost);
        System.out.println("What type?");
        this.typeChocolate = new Scanner(System.in).next();
        System.out.println("What per?");
        this.percentageCocoa = Integer.valueOf(new Scanner(System.in).next());
    }


    Chocolate(String name, int weight, int cost, int percentageCocoa, String typeChocolate){

        super(name, weight, cost);

        this.percentageCocoa = percentageCocoa;

        this.typeChocolate = typeChocolate;

    }

    //public int getWeight() {return 12;}
    //public int getCost() {return 120; }

    int getPercentageCocoa(){

        return this.percentageCocoa;
    }

    String getTypeChocolate(){

        return this.typeChocolate;
    }

    public String print() {
        return super.print() + " Тип: " + this.typeChocolate + " Процент какао: " + this.percentageCocoa;
    }


}
