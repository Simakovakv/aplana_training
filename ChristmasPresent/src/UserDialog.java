import java.util.Scanner;

public class UserDialog {

    private Scanner reader = new Scanner(System.in);

    private BoxBehavior box;

    UserDialog() {
        do {
            System.out.println("Давайте собирать коробку с подарками.");

            askWeightAndCreateBox();

            System.out.println("Вы хотите собрать коробку по весу(1) или самую презентабельную(2)?");

            switch (reader.next()) {

                case "1":

                    do {
                        String name = askNameOfPresent();

                        String count = askCountOfPresent();

                        addToBoxWhen1(count, name);

                    }

                    while (oneMore("подарок"));

                    printCommonResult();

                    printResultList();

                    break;


                case "2":
                    String count;

                    do {
                        String name = askNameOfPresent();

                        count = askCountOfPresent();

                        addToBoxWhen2(count, name);

                    }

                    while (oneMore("подарок"));

                    getBestPresent();

                    printCommonResult();

                    break;

                default:

                    System.out.println("Такого кейса не существует, попробуйте еще раз");

                    break;
            }
        }
        while (oneMore("коробку")==true);
    }


    private String askNameOfPresent(){
        System.out.println("Выберите тип подарка(Шоколад(1), конфета(2), пироженое(3), " +
                "чупа-чупс(4), мармелад(5)");
        return reader.next();

    }

    private String askCountOfPresent(){
        System.out.print("и его количество: ");
        return reader.next();
    }

    private BoxBehavior askWeightAndCreateBox(){

        System.out.println("Укажите ее максимальный вес (в граммах): ");
        box = FactoryBox.createBox(Integer.valueOf(reader.next()));
        return box;
    }

    private void addToBoxWhen1 (String count, String name) {
        for (int i = 0; i < Integer.valueOf(count); i++) {
            System.out.printf("Введите параметры %d продукта\n", i + 1);
            BasePresentBehavior newPresent = FactoryPresent.createPresent(name);

            if (box.checkWeight(newPresent))
                box.appendToList(newPresent);
            else break;
        }
    }

    private void addToBoxWhen2 (String count, String name) {
        for (int i = 0; i < Integer.valueOf(count); i++) {
            System.out.printf("Введите параметры %d продукта\n", i + 1);
            BasePresentBehavior newPresent = FactoryPresent.createPresent(name);
            box.appendToList(newPresent);
        }
    }

    private void getBestPresent() {
        System.out.println("Лучший подарок:");
        box.chooseBestCost();
    }

    private boolean oneMore(String name){
        System.out.printf("Хотите добавить еще %s? y/n\n", name);
        switch (reader.next()) {
            case "y":
                return true;
            case "n":
                return false;
            default:
                return oneMore(name);
        }
    }

    private void printCommonResult(){
        System.out.println("Всего подарков: " + box.getCount());
        System.out.println("Общий вес подарков: " + box.getCommonWeight());
        System.out.println("Общая стоимость подарков: " + box.getCommonCost());

    }

    private void printResultList(){
        System.out.println("Вы выбрали следующие продукты:\n" ); box.display();
    }


}
