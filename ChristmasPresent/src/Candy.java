import java.util.Scanner;

public class Candy extends BasePresent {

    private String typeCandy;

    Candy(String name){
        super(name);
        System.out.println("Введите тип конфеты:");
        this.typeCandy = new Scanner(System.in).next();

    }

    Candy(String name, int weight, int cost, String typeCandy){

        super(name, weight, cost);

        this.typeCandy = "Truffle";

    }

    String getTypeCandy(){

        return this.typeCandy;

    }

    public String print() {
        return super.print() + " Тип:" + this.typeCandy;
    }

}
