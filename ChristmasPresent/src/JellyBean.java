import java.util.Scanner;

public class JellyBean extends BasePresent {

    private String typeJellybean;

    private String taste;

    JellyBean(String name){
        super(name);
        System.out.println("Введите тип:");
        this.typeJellybean = new Scanner(System.in).next();
        System.out.println("Введите вкус:");
        this.taste = new Scanner(System.in).next();
    }

    JellyBean(String name, int weight, int cost, String typeJellybean, String taste){

        super(name, weight, cost);

        this.typeJellybean = "Cubes";

        this.taste = "Apple";

    }

    String getTypeJellybean(){

        return this.typeJellybean;
    }

    String getTaste(){

        return this.taste;

    }
    public String print() {
        return super.print() + " Вкус: " + this.taste + " Тип: " + this.typeJellybean;
    }

}
