import java.util.ArrayList;

public interface BoxBehavior {
    int getCommonWeight();
    //int getMinWeight();
    int getMaxWeight();
    int getCommonCost();
    //int getMaxCost();
    //int getMinCost();
    boolean checkWeight(BasePresentBehavior newPresent);
    int getCount();
    boolean appendToList(BasePresentBehavior basePresent);
    void chooseBestCost();
    /*String*/void display();
}
