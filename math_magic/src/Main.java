/**
 * This interactive math game with user.
 * User have to input date of birthday but first do some math manipulations
 * and this programm have to guess this date.
 *
 * @autor Simakova Ksenia
 * @version 1.0
 *
 * @param args Not using.
 *
 * @exception DateTimeParseException Exception when @link DateCompare.transformToDate()
 * parsing String input data which is not match to date
 * @see java.time.format.DateTimeParseException
 *
 * @exception NumberFormatException when @link UserDialog.getInput get not number data
 * @see java.lang.NumberFormatException
 *
 * Input: 1960 (17 Oct, current day)
 * Output: Вы родились: 17 Октября
 * По зодиаку вы Весы
 * Поздравляю, у вас сегодня день рождения!
 *
 * Input: 454 (2 April)
 * Output: Вы родились: 2 Апреля
 * По зодиаку вы Овен
 * До вашего дня рождения осталось 167 дней
 */

import java.time.format.DateTimeParseException;

public class Main {

    public static void main (String[] args) {

        do  {
            try {
                System.out.println(UserDialog.ask());

                int input = Integer.parseInt(UserDialog.getInput());

                UserDialog.getBDay(input);

                if (UserDialog.isDate()) {
                    System.out.println("Вы родились:" + " " + UserDialog.date.getDayOfMonth() + " " + UserDialog.getMonth(UserDialog.date.getMonthValue()));
                } else
                    System.out.println("Error");

                System.out.println("По зодиаку вы " + UserDialog.getZodiac());

                if (UserDialog.isBDay()) {
                    System.out.println("Поздравляю, у вас сегодня день рождения!");
                } else
                    System.out.printf("До вашего дня рождения осталось %d дней \n\n", UserDialog.countToBDays());

            } catch (DateTimeParseException exc) {
                System.out.printf("Вы ввели неверное число, посчитайте и введите число еще раз (data is not parsable!) " + "\n");
            }
            catch (NumberFormatException nfe) {
                System.out.printf("Вы ввели не число, попробуйте еще раз" + "\n");
            }
        }
        while (UserDialog.oneMore());
    }
}
