/**
 *   Class to provide a dialog with user and to give him answers
 */

import java.time.temporal.ChronoUnit;
import java.util.Scanner;
import java.time.*;

public class UserDialog {

    static Scanner reader;
    static int day = 0;
    static int month = 0;
    static final String[] MonthName = {"Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа",
            "Сентября", "Октября", "Ноября", "Декабря"};
    static final Zodiac[] z_array = new Zodiac[] {new Zodiac("21.03", "20.04", "Овен"), new Zodiac("21.04", "20.05", "Телец")
            , new Zodiac("21.05", "21.06", "Близнецы"), new Zodiac("22.06", "22.07", "Рак")
            , new Zodiac("23.07", "23.08", "Лев"), new Zodiac("24.08", "23.09", "Дева")
            , new Zodiac("24.09", "23.10", "Весы"), new Zodiac("24.10", "22.11", "Скорпион")
            , new Zodiac("23.11", "21.12", "Стрелец"), new Zodiac("22.12", "20.01", "Козерог")
            , new Zodiac("21.01", "20.02", "Водолей"), new Zodiac("21.02", "20.03", "Рыбы")};
    static LocalDate date;
    static DateCreater dateBDay;
    static LocalDate today = LocalDate.now();
    static String ask() {
        return "Сейчас я угадаю дату твоего рождения!" + "\n" + "1 - Умножьте число дня своего рождения на 2" + "\n" +
                "2 - затем прибавьте 5" + "\n" + "3 - умножьте получившееся число на 50" + "\n" +
                "4 - к этому результату прибавьте номер месяца своего рождения" + "\n" + "Введите полученное значение: ";
    }

    /**
     * Get name of month from array MonthName with number of month as index-1
     * @return get name of month
     */

    static String getMonth(int digit) {
        return MonthName[digit-1];
    }

    /**
     * Get input data from user
     * @return input data
     */

    static String getInput() {
        /*Scanner */reader = new Scanner(System.in);
        return reader.next();
    }
    /**
     * Function to calculate month and day of B-day from input data @see UserDialog#getInput() and get date as LocalDate
     * @return LocalDate date of birthday
     */

    static LocalDate getBDay (int num) {
        int result = num - 250;
        month = result % 100;
        result /= 100;
        day = result % 100;
        dateBDay = new DateCreater(day, month);
        date = dateBDay.transformToDate();
        return date;
    }

    /**
     * Function to get Zodiac name via for-each loop on Zodiac array
     * @return name of Zodiac
     */

    static String getZodiac(){
        for (Zodiac z : z_array) {
            DateCreater dateBefore = new DateCreater(z.end);
            LocalDate end = dateBefore.transformToDate().plusDays(1);
            DateCreater dateAfter = new DateCreater(z.start);
            LocalDate start = dateAfter.transformToDate().minusDays(1);
            if (date.isBefore(end) && date.isAfter(start))
                return z.name;
        }
        return "Введена некорректная дата";
    }

    /**
     * Function to check that Date is not null
     * @return answer that date is exist
     */

    static boolean isDate() {
        return date != null;
    }

    /**
     * Function to check that result from getBDay method @see UserDialog#getBDay(int num) is equals current day
     * @return answer that date is B-day
     */

    static boolean isBDay() {
        if (date.equals(today))
            return true;
        else
            return false;
    }

    /**
     * Function to get count of days from today until next B-day using increase current result on 1 year
     * @return number of days
     */

    static int countToBDays () {
        if (date.isAfter(today))
            return (int) ChronoUnit.DAYS.between(today, date);
        else
        {
            LocalDate nextBDay = date.withYear(today.getYear()).plusYears(1);
            return (int) ChronoUnit.DAYS.between(today, nextBDay);
        }

    }

    /**
     * Function to check that user want to continue or not via y/n key, or ask again if user set wrong key
     * @return user answer
     */

    static boolean oneMore(){
        System.out.println("Хотите попробовать еще? (Если да - нажмите y, если нет - n, аварийный выход Ctrl + C)");
        String variant = reader.next();
        switch (variant){
            case "y" :
                return true;
            case "n" :
                return false;
            default : return oneMore();
        }
    }
}
