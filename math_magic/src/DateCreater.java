/**
 * This class helps to format Date with 2 integer variables as day and month and
 * 1 string variable as String from Zodiac example field
 */

import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.util.Locale;

public class DateCreater {
    String month;
    String day;


    DateCreater(int day, int month) {
        this.month = Integer.toString(month);
        this.day = Integer.toString(day);
    }
    DateCreater(String dayMonth) {
        this.month = dayMonth.substring(dayMonth.length()-2);
        this.day = dayMonth.substring(dayMonth.length()-5, dayMonth.length()-3);
    }
    LocalDate transformToDate() {
            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("d.M.yyyy", Locale.US);
            LocalDate date = LocalDate.parse(this.day + "." + this.month + "." + LocalDate.now().getYear(), dateFormat);
            if (date.isAfter(LocalDate.now()))
                return date;
            else
            return date.plusYears(1);
    }
}

