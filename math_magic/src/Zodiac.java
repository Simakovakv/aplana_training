/**
 * This class describes Zodiac with <b>start</b> and <b>end</b> variables which are boundaries of zodiac interval,
 * and RUS <b>name</b> of Zodiac
 */

public class Zodiac {
        String start;
        String end;
        String name;
    /**
     * Constructor Zodiac
     * @param start first boundary
     * @param end last boundary
     * @param name name of Zodiac
     */

    public Zodiac(String start, String end, String name) {
        this.start = start;
        this.end = end;
        this.name = name;
    }
}
