package org.aplana.home.factories;

import org.aplana.home.sweets.Candy;

public class CandyFactory implements SweetFactory {

    @Override
    public Candy create(float price, float weight, Enum uniqueParameter) {

        return new Candy(price, weight, uniqueParameter);

    }
}
