package org.aplana.home.factories;

import org.aplana.home.sweets.BaseSweet;

@FunctionalInterface
public interface SweetFactory<S extends BaseSweet> {

    S create(float price, float weight, Enum uniqueParameter);

}


