package org.aplana.home.factories;

import org.aplana.home.sweets.Chocolate;

public class ChocolateFactory implements SweetFactory {

    @Override
    public Chocolate create(float price, float weight, Enum uniqueParameter) {

        return new Chocolate(price, weight, uniqueParameter);

    }
}
