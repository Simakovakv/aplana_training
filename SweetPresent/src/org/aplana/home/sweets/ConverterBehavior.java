package org.aplana.home.sweets;

public interface ConverterBehavior<T,R> {

    R convert(T fromType);
}
