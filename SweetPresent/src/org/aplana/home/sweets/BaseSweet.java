package org.aplana.home.sweets;


public abstract class BaseSweet {

    private Enum uniqueParameter;

    private float price, weight;

    BaseSweet(float price, float weight, Enum uniqueParameter ){

        this.price = price;
        this.weight = weight;
        this.uniqueParameter = uniqueParameter;
    }

    BaseSweet(float price, float weight ) {

        this(price, weight, Size.SMALL);
    }

    BaseSweet(float price) {

        this(price, 0);
    }

    BaseSweet(){

        this(0 );

    }

    public Enum getUniqueParameter() {
        return uniqueParameter;
    }

/*    public void setUniqueParameter(Enum uniqueParameter) {
        this.uniqueParameter = uniqueParameter;
    }*/

    public float getPrice() {

        return price;

    }

     public float getWeight() {

        return weight;

    }

    @Override
    public String toString() {

        return String.format("%s : вес = %s, цена = %s, %s\n",
                this.getClass().getSimpleName(), getWeight(), getPrice(), getUniqueParameter());

    }
}
