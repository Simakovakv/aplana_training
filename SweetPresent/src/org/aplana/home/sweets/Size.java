package org.aplana.home.sweets;

public enum Size {SUPERBIG, BIG, MEDIUM, SMALL, SUPERSMALL;}