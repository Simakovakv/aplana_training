package org.aplana.home;

import org.aplana.home.sweets.BaseSweet;

public interface CheckPresent<T extends BaseSweet> {

    boolean check(T present);

}