package org.aplana.home;

import org.aplana.home.factories.CandyFactory;
import org.aplana.home.factories.ChocolateFactory;
import org.aplana.home.factories.SweetFactory;
import org.aplana.home.sweets.*;



public class App {

    public static void main(String[] args) {

        System.out.println("hello");

        //Task 1: 2 class implements interface and 2 class with interface and link on constructor(new) using condition with add present

        Box box = new Box(1500);
        SweetFactory<BaseSweet> sweetFactory1 = Cookie::new;
        BaseSweet cookie1 = sweetFactory1.create(100, 200, Size.BIG);
        BaseSweet cookie2 = sweetFactory1.create(170, 270, Size.MEDIUM);

        SweetFactory<BaseSweet> sweetFactory2 = Jellybean::new;
        BaseSweet jellybean1 = sweetFactory2.create(300, 250, Size.SUPERBIG);
        BaseSweet jellybean2 = sweetFactory2.create(300, 250, Size.SMALL);

        SweetFactory<BaseSweet> sweetFactory3 = Chocolate::new;
        BaseSweet chocolate3 = sweetFactory3.create(300, 250, Size.MEDIUM);
        BaseSweet chocolate4 = sweetFactory3.create(300, 250, Size.SUPERBIG);

        ChocolateFactory chocolateFactory = new ChocolateFactory();
        Chocolate chocolate1 = chocolateFactory.create(100, 200, Size.BIG);
        Chocolate chocolate2 = chocolateFactory.create(150, 400, Size.SMALL);

        CandyFactory candyFactory = new CandyFactory();
        Candy candy1 = candyFactory.create(50,200,Size.MEDIUM);
        Candy candy2 = candyFactory.create(70, 250, Size.SUPERSMALL);

        box.addTo(chocolate1);
        box.addTo(chocolate2);

        //Task 2: set (auto when start) and add Policy
        box.addPolicy(sweet -> sweet.getPrice() < 300);

        box.addTo(chocolate3);
        box.addTo(candy1);
        box.addTo(candy2);
        box.addTo(cookie1);
        box.addTo(cookie2);
        box.addTo(jellybean1);
        box.addTo(jellybean2);
        box.addTo(chocolate4);

        //Task 4: use StreamAPI from box

        box.printOnly("Chocolate");
        System.out.println("Всего шоколадок: " + box.getNumEach("Chocolate"));

        // Collections.sort(box.sweetList, (x1, x2) -> (int) x2.getWeight() - (int) x1.getWeight());

        //box.deleteFrom(chocolate1);

        System.out.println(box);

    }
}
