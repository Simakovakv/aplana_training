package org.aplana.home;

import org.aplana.home.sweets.BaseSweet;
import org.aplana.home.sweets.ConverterBehavior;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

class Box {

    private float commonPrice = 0;

    private float commonWeight = 0;

    private float maxWeight = 0;

    //Task3: Converter
    ConverterBehavior<Float, Double> converterToUSD = price -> getCommonPrice() / 65.64;

    ConverterBehavior<Float, Double> converterToEuro = price -> getCommonPrice() / 75.0;

    Box(float maxWeight) {

        this.maxWeight = maxWeight;

    }

    List<BaseSweet> sweetList = new ArrayList<BaseSweet>();

    /**
     * checkSweet check that weight after add of present will be not more than max weight of org.aplana.home.Box
     */
    Predicate<BaseSweet> checkSweet = sweet -> getCommonWeight()+sweet.getWeight() < getMaxWeight();

    public float getCommonWeight(){

        return commonWeight;

    }

    public float getCommonPrice() {

        return commonPrice;

    }

    public float getMaxWeight(){
        return maxWeight;
    }

    public void addTo(BaseSweet baseSweet){

        if(checkSweet.test(baseSweet)){

            sweetList.add(baseSweet);

            commonWeight += baseSweet.getWeight();

            commonPrice += baseSweet.getPrice();
        }
        else System.out.printf("Перевес, подарок %s не добавлен\n", baseSweet.getClass().getSimpleName());

    }

    public void deleteFrom(BaseSweet baseSweet) {

        if(sweetList.remove(baseSweet)){

            commonWeight -= baseSweet.getWeight();

            commonPrice -= baseSweet.getPrice();

        }

    }

    public void printOnly(String className){
        System.out.printf("Перечисление %s:\n", className);
        sweetList.stream()
                .filter(s -> s.getClass().getSimpleName().equals(className))
                .forEach(System.out::println);
                //.count();
    }

    public String getNumEach(String className){
        return String.valueOf(sweetList.stream()
                .filter(s -> s.getClass().getSimpleName().equals(className))
                .count());

    }

    public void setPolicy(Predicate<BaseSweet> predicate) {
        checkSweet = predicate;
    }

    public void addPolicy(Predicate<BaseSweet> predicate) {
        checkSweet = checkSweet.and(predicate);
    }

    @Override
    public String toString() {

        return String.format("Продукты:\n%s\n%s: Общий вес = %g, общая стоимость = %.2f руб.(%.2f USD, %.2f EURO), всего продуктов: %d ",
                sweetList, this.getClass().getSimpleName(), getCommonWeight(), getCommonPrice(),
                converterToUSD.convert(getCommonPrice()), converterToEuro.convert(getCommonPrice()), sweetList.size());

    }

}
